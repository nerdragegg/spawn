package kv

import (
	"encoding/json"
	"errors"
	"fmt"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/nerdragegg/spawn/events"
	"bitbucket.org/nerdragegg/spawn/matches"
	etcd "github.com/coreos/etcd/client"
	"golang.org/x/net/context"
)

// SpawnKeyPrefix is the key prefix for spawn agents and matches in etcd
const SpawnKeyPrefix = "/spawn/"

// EtcdAgentPathPrefix constructs a path prefix from location and agent IDs.
func EtcdAgentPathPrefix(location, agentID string) (prefix string) {
	return path.Join(SpawnKeyPrefix, location, "agents", agentID) + "/"
}

// NewEtcdKVStore initializes a Storer using etcd.
func NewEtcdKVStore(client etcd.Client) *EtcdKVStore {
	return &EtcdKVStore{
		etcd: client,
	}
}

// EtcdKVStore implements kv.Storer for etcd.
type EtcdKVStore struct {
	etcd etcd.Client
}

// RegisterAgent registers an agent in etcd
func (e *EtcdKVStore) RegisterAgent(ctx context.Context, ar AgentRegistration) (err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	options := &etcd.SetOptions{
		Dir: true,
		TTL: ar.TTL,
	}
	prefix := EtcdAgentPathPrefix(ar.Location, ar.ID)
	_, err = keysAPI.Set(ctx, prefix, "", options)
	if err != nil {
		return
	}

	_, err = keysAPI.Set(ctx, path.Join(prefix, "network", "ipv4"), ar.PublicIPV4, &etcd.SetOptions{})
	if err != nil {
		return
	}
	return
}

func (e *EtcdKVStore) registerContainer(ctx context.Context, keysAPI etcd.KeysAPI, container ContainerRegistration) (err error) {
	key := path.Join(EtcdAgentPathPrefix(container.Location, container.Agent), "containers", container.ID)
	_, err = keysAPI.Set(ctx, key, "", &etcd.SetOptions{Dir: true, TTL: container.TTL})
	if err != nil {
		return
	}
	return
}

// SyncAgent ensures agent state in etcd. Updates agent registration as well as
// containers managed by the agent.
func (e *EtcdKVStore) SyncAgent(ctx context.Context, state AgentState) (err error) {
	err = e.RegisterAgent(ctx, state.Registration)
	if err != nil {
		return
	}

	errs := []string{}
	keysAPI := etcd.NewKeysAPI(e.etcd)
	for _, container := range state.Containers {
		err = e.registerContainer(ctx, keysAPI, container)
		if err != nil {
			// eat the error, we want to register as many as possible
			errs = append(errs, err.Error())
		}
	}
	if len(errs) != 0 {
		err = fmt.Errorf("kv: errors registering containers: %s", strings.Join(errs, ", "))
		return
	}
	return
}

// PrepareEvent reads a response from etcd and returns a MatchEvent
func PrepareEvent(matchPrefix string, response *etcd.Response, match *matches.Match) (event *events.MatchEvent) {
	event = new(events.MatchEvent)
	event.Match = match

	if response == nil || match == nil {
		event.Scenario = events.MatchStateUnknown
		return
	}

	if response.Node.Dir && response.Node.Key == matchPrefix {
		switch response.Action {
		case "create", "set":
			event.Scenario = events.MatchCreated
			return
		case "delete", "expire":
			event.Scenario = events.MatchDeleted
			return
		default:
			event.Scenario = events.MatchStateUnknown
			return
		}
	}

	if response.Node.Key == path.Join(matchPrefix, "agent") {
		switch response.Action {
		case "create", "set":
			event.Scenario = events.MatchAssigned
			return
		case "delete", "expire":
			event.Scenario = events.MatchUnassigned
			return
		default:
			event.Scenario = events.MatchStateUnknown
			return
		}
	}
	return
}

// WatchMatch watches a match for updates
func (e *EtcdKVStore) WatchMatch(ctx context.Context, request *WatchMatchRequest) (err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, request.Match.Location, "matches", request.Match.ID)
	watcher := keysAPI.Watcher(key, &etcd.WatcherOptions{Recursive: true})
	var resp *etcd.Response
	for {
		resp, err = watcher.Next(ctx)
		if err != nil {
			return
		}

		if resp.Action == "delete" && resp.Node.Key == key {
			// watched path has been deleted
			return
		}

		event := PrepareEvent(key, resp, request.Match)
		if event.Scenario == events.MatchStateUnknown {
			continue
		}

		request.Events <- event

		select {
		case <-ctx.Done():
			return
		default:
			continue
		}
	}
	return
}

// LockMatch grabs lock on a match
func (e *EtcdKVStore) LockMatch(ctx context.Context, request LockMatchRequest) (ok bool, err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, request.Location, "matches", request.Match, "agent")
	options := &etcd.SetOptions{
		PrevExist: etcd.PrevNoExist,
		TTL:       request.LockTTL,
	}

	_, err = keysAPI.Set(ctx, key, request.Agent, options)
	if err != nil {
		// if the node already exists, then another agent already grabbed lock
		// on this match. Eat the error and return false.
		if GetEtcdErrorCode(err) == etcd.ErrorCodeNodeExist {
			err = nil
		}
		return
	}
	ok = true
	return
}

// UnlockMatch releases lock on a match
func (e *EtcdKVStore) UnlockMatch(ctx context.Context, request UnlockMatchRequest) (err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, request.Location, "matches", request.Match, "agent")
	options := &etcd.DeleteOptions{
		PrevValue: request.Agent,
	}
	_, err = keysAPI.Delete(ctx, key, options)
	if err != nil {
		return
	}
	return
}

// ListAllMatches returns all matches in etcd for specified location
func (e *EtcdKVStore) ListAllMatches(ctx context.Context, location string) (ms []*matches.Match, err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, location, "matches")
	resp, err := keysAPI.Get(ctx, key, &etcd.GetOptions{Quorum: true})
	if err != nil {
		return
	}

	if resp.Node == nil {
		return
	}

	ms = make([]*matches.Match, len(resp.Node.Nodes))
	for i, v := range resp.Node.Nodes {
		ms[i] = new(matches.Match)
		err = json.Unmarshal([]byte(v.Value), ms[i])
		if err != nil {
			err = fmt.Errorf("spawn: error marshalling match data: %s", err.Error())
			return
		}
	}
	return
}

// GetMatch returns specified match in etcd
func (e *EtcdKVStore) GetMatch(ctx context.Context, location, matchID string) (match *matches.Match, err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, location, "matches", matchID, "match")
	resp, err := keysAPI.Get(ctx, key, &etcd.GetOptions{Quorum: true})
	if err != nil {
		return
	}
	if resp.Node == nil {
		err = errors.New("spawn: match value is empty")
		return
	}

	match = new(matches.Match)
	err = json.Unmarshal([]byte(resp.Node.Value), match)
	if err != nil {
		err = fmt.Errorf("spawn: error marshalling match data: %s", err.Error())
		return
	}
	return
}

// GetEtcdErrorCode retrieves error code from error
func GetEtcdErrorCode(err error) int {
	if err == nil {
		return 0
	}

	if e, ok := err.(etcd.Error); ok {
		return e.Code
	}
	return 0
}

// RegisterContainer registers a container with etcd
func (e *EtcdKVStore) RegisterContainer(ctx context.Context, request RegisterContainerRequest) (err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	dirKey := path.Join(SpawnKeyPrefix, request.Location, "agents", request.Agent, "containers", request.Container)
	_, err = keysAPI.Set(ctx, dirKey, "", &etcd.SetOptions{
		PrevExist: etcd.PrevNoExist,
		TTL:       request.TTL,
		Dir:       true,
	})
	if err != nil {
		return
	}

	portKey := path.Join(SpawnKeyPrefix, request.Location, "agents", request.Agent, "containers", request.Container, "port")
	_, err = keysAPI.Set(ctx, portKey, strconv.FormatInt(request.Port, 10), &etcd.SetOptions{
		PrevExist: etcd.PrevNoExist,
		TTL:       request.TTL,
		Dir:       true,
	})
	if err != nil {
		return
	}
	return
}

// DeregisterContainer removes a container from etcd
func (e *EtcdKVStore) DeregisterContainer(ctx context.Context, request DeregisterContainerRequest) (err error) {
	keysAPI := etcd.NewKeysAPI(e.etcd)
	key := path.Join(SpawnKeyPrefix, request.Location, "agents", request.Agent, "containers", request.Container)
	_, err = keysAPI.Delete(ctx, key, &etcd.DeleteOptions{Recursive: true, Dir: true})
	if err != nil {
		if GetEtcdErrorCode(err) == etcd.ErrorCodeKeyNotFound {
			// it's not there, whatever
			err = nil
		}
		return
	}
	return
}
