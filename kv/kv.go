package kv

import (
	"time"

	"bitbucket.org/nerdragegg/spawn/events"
	"bitbucket.org/nerdragegg/spawn/matches"
	"golang.org/x/net/context"
)

// AgentRegistration holds information needed to register an agent.
type AgentRegistration struct {
	ID         string
	Location   string
	PublicIPV4 string
	TTL        time.Duration
}

// ContainerRegistration holds information about a container
type ContainerRegistration struct {
	ID       string
	Location string
	Agent    string
	Port     int64
	TTL      time.Duration
}

// AgentState holds current state of an agent.
type AgentState struct {
	Registration AgentRegistration
	Containers   []ContainerRegistration
}

// LockMatchRequest contains parameters needed for locking a match
type LockMatchRequest struct {
	// location ID
	Location string

	// agent ID
	Agent string

	// match ID
	Match   string
	LockTTL time.Duration
}

// UnlockMatchRequest contains parameters needed for unlocking a match
type UnlockMatchRequest struct {
	// location ID
	Location string

	// agent ID
	Agent string

	// match ID
	Match string
}

// WatchMatchRequest contains parameters needed for watching a match
type WatchMatchRequest struct {
	Events chan *events.MatchEvent
	Match  *matches.Match
}

// RegisterContainerRequest contains parameters for registering a container
type RegisterContainerRequest struct {
	Container string
	Port      int64
	TTL       time.Duration
	Location  string
	Agent     string
	Match     string
}

// DeregisterContainerRequest contains paramters for deregistering a container
type DeregisterContainerRequest RegisterContainerRequest

// Storer is the abstraction layer for a key value store that provides
// locking and service discovery.
type Storer interface {
	RegisterAgent(ctx context.Context, ar AgentRegistration) (err error)
	SyncAgent(ctx context.Context, state AgentState) (err error)

	WatchMatch(ctx context.Context, request *WatchMatchRequest) (err error)
	LockMatch(ctx context.Context, request LockMatchRequest) (ok bool, err error)
	UnlockMatch(ctx context.Context, request UnlockMatchRequest) (err error)
	ListAllMatches(ctx context.Context, location string) (matches []*matches.Match, err error)
	GetMatch(ctx context.Context, location, matchID string) (match *matches.Match, err error)

	RegisterContainer(ctx context.Context, request RegisterContainerRequest) (err error)
	DeregisterContainer(ctx context.Context, request DeregisterContainerRequest) (err error)
}
