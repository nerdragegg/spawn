package spawn

import (
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/nerdragegg/spawn/containers"
	"bitbucket.org/nerdragegg/spawn/events"
	"bitbucket.org/nerdragegg/spawn/kv"
	"bitbucket.org/nerdragegg/spawn/matches"

	"golang.org/x/net/context"

	"github.com/Sirupsen/logrus"
	etcd "github.com/coreos/etcd/client"
	"github.com/fsouza/go-dockerclient"
	"github.com/kelseyhightower/envconfig"
	"github.com/satori/go.uuid"
)

// Logger is the package level logger
var Logger *logrus.Logger

// Default TTLs
var (
	DefaultContainerTTL = time.Duration(5 * time.Minute)
)

// AgentConfig holds config options for the spawn agent
type AgentConfig struct {
	Images          []string      `json:"images,omitempty" yaml:"images" default:"urbanterror/vanilla"`
	EtcdHosts       []string      `json:"etcd_host,omitempty" yaml:"etcd_host" default:"etcd:2379,etcd:4001"`
	RegistrationTTL time.Duration `json:"registration_ttl,omitempty" yaml:"registration_ttl" default:"1h"`
	Location        string        `json:"location,omitempty" yaml:"location" required:"true"`
	ID              string        `json:"id,omitempty", yaml:"id" required:"false"`
}

// Agent implements spawn agent
type Agent struct {
	docker *docker.Client
	kv     kv.Storer
	cs     containers.Service
	router events.MatchEventRouter
	config *AgentConfig
	id     string
	events chan *events.MatchEvent
	ctx    context.Context
}

// BuildAgent returns a new configured agent
func BuildAgent() (agent *Agent, err error) {
	Logger = &logrus.Logger{
		Out:       os.Stdout,
		Formatter: &logrus.JSONFormatter{},
		Level:     logrus.InfoLevel,
	}

	config := new(AgentConfig)
	err = envconfig.Process("spawn_agent", config)
	if err != nil {
		Logger.Error(err)
		return
	}
	if config.ID == "" {
		config.ID = uuid.NewV4().String()
		Logger.Debugf("spawn: agent ID not provided, using %s", config.ID)
	}

	agent = new(Agent)
	agent.config = config
	agent.id = agent.config.ID

	dockerClient, err := docker.NewClientFromEnv()
	if err != nil {
		Logger.Error(err)
		return
	}
	agent.docker = dockerClient

	etcdClient, err := etcd.New(etcd.Config{
		Endpoints: agent.config.EtcdHosts,
		Transport: etcd.DefaultTransport,
	})
	if err != nil {
		Logger.Error(err)
		return
	}
	agent.kv = kv.NewEtcdKVStore(etcdClient)

	cs, err := containers.New()
	if err != nil {
		Logger.Errorf("spawn: error configuring ContainerServer: %s", err.Error())
		return
	}
	agent.cs = cs

	agent.events = make(chan *events.MatchEvent)
	agent.router = events.DefaultMatchEventRouter
	agent.router.Register(events.MatchCreated, agent.HandleCreatedMatch)
	agent.router.Register(events.MatchUnassigned, agent.HandleLockableMatch)
	agent.router.Register(events.MatchAssigned, agent.HandleAssignedMatch)
	agent.router.Register(events.MatchDeleted, agent.HandleDeletedMatch)
	agent.ctx = context.Background()
	return
}

// Run starts the main loop
func (a *Agent) Run() (err error) {
	ip, err := queryICanHazIP()
	if err != nil {
		Logger.Errorf("spawn: unable to retrieve public address: %s", err.Error())
		return
	}

	reg := kv.AgentRegistration{
		ID:         a.id,
		Location:   a.config.Location,
		TTL:        a.config.RegistrationTTL,
		PublicIPV4: ip,
	}
	err = a.kv.RegisterAgent(a.ctx, reg)
	if err != nil {
		Logger.Errorf("spawn: failed to register agent: %s", err.Error())
		return
	}

	ms, err := a.kv.ListAllMatches(a.ctx, a.config.Location)
	if err != nil {
		Logger.Errorf("spawn: failed to list matches: %s", err.Error())
		return
	}

	for i, m := range ms {
		go func(m *matches.Match) {
			request := &kv.WatchMatchRequest{
				Events: a.events,
				Match:  m,
			}
			err := a.kv.WatchMatch(a.ctx, request)
			if err != nil {
				Logger.Errorf("spawn: unable to watch match %s: %s", m.ID, err.Error())
			}
		}(m)

		req := kv.LockMatchRequest{
			Match:    m.ID,
			Location: a.config.Location,
			Agent:    a.id,
		}
		locked, err := a.kv.LockMatch(a.ctx, req)
		if err != nil {
			Logger.Errorf("spawn: failed to lock match: %s", err.Error())
			err = nil
			continue
		}
		if !locked {
			Logger.Infof("spawn: match %s already locked by agent %s", m.ID, a.id)
			continue
		}
		ms[i].Agent = a.id
		e := &events.MatchEvent{
			Match:    ms[i],
			Scenario: events.MatchAssigned,
		}
		a.events <- e
	}

	for {
		select {
		case event := <-a.events:
			err = a.router.HandleEvent(event)
			if err != nil {
				Logger.Errorf("spawn: error handling event: %s", err.Error())
				err = nil
			}
		}
	}
	return
}

func queryICanHazIP() (ip string, err error) {
	resp, err := http.Get("https://icanhazip.com")
	if err != nil {
		return
	}
	defer resp.Body.Close()
	b := make([]byte, resp.ContentLength)
	_, err = io.ReadFull(resp.Body, b)
	if err != nil {
		return
	}

	val := strings.TrimSpace(string(b))
	addr, err := net.ResolveIPAddr("ip", val)
	if err != nil {
		return
	}
	ip = addr.String()
	return
}
