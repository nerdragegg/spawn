package containers

import (
	"fmt"
	"strconv"

	"bitbucket.org/nerdragegg/spawn/games"
	"bitbucket.org/nerdragegg/spawn/matches"
	"github.com/fsouza/go-dockerclient"
)

// StopServerTimeout is the number of seconds docker should wait before timing
// out when stopping a container
const StopServerTimeout uint = 5

// DefaultLabelFilter contains filters for returning only urbanterror
var DefaultLabelFilter = map[string]string{
	"biz.nerdrage.game": games.UrbanTerror,
}

// ListContainerRequest holds parameters for ListContainer
type ListContainerRequest struct {
	Labels map[string]string
}

// RunContainerResponse is returned after starting a container
type RunContainerResponse struct {
	ContainerID string `json:"container_id,omitempty"`
	Port        int64  `json:"port,omitempty"`
}

// Service describes the API for providing docker containers
type Service interface {
	ListContainers(request ListContainerRequest) ([]docker.APIContainers, error)
	Run(match *matches.Match) (response RunContainerResponse, err error)
	Stop(containerID string) error
}

type containerServer struct {
	docker *docker.Client
}

// New instantiates a new Service. Labels are applied to all containers
// on creation.
func New() (cs Service, err error) {
	client, err := docker.NewClientFromEnv()
	if err != nil {
		return
	}
	cs = &containerServer{
		docker: client,
	}
	return
}

// ListContainers retrieves list of containers in docker
func (cs *containerServer) ListContainers(request ListContainerRequest) (c []docker.APIContainers, err error) {
	filters := make(map[string]string)
	for k, v := range DefaultLabelFilter {
		filters[k] = v
	}
	for k, v := range request.Labels {
		filters[k] = v
	}

	labels := make([]string, 0, len(filters))
	for k, v := range filters {
		labels = append(labels, fmt.Sprintf("%s=%s", k, v))
	}
	options := docker.ListContainersOptions{
		Filters: map[string][]string{
			"label": labels,
		},
	}
	c, err = cs.docker.ListContainers(options)
	if err != nil {
		return
	}
	return
}

func prepareExposedPorts(match *matches.Match) map[docker.Port]struct{} {
	opts := matches.GetMatchServerOptions(match)
	exposedPorts := make(map[docker.Port]struct{})
	for port, proto := range opts.Ports {
		p := fmt.Sprintf("%d/%s", port, proto)
		exposedPorts[docker.Port(p)] = struct{}{}
	}
	return exposedPorts
}

func prepareContainer(match *matches.Match) docker.CreateContainerOptions {
	labels := make(map[string]string)
	for k, v := range DefaultLabelFilter {
		labels[k] = v
	}
	labels["com.ftwgl.match"] = match.ID
	return docker.CreateContainerOptions{
		Name: match.ID,
		Config: &docker.Config{
			ExposedPorts: prepareExposedPorts(match),
			Labels:       labels,
		},
		HostConfig: &docker.HostConfig{
			PublishAllPorts: true,
		},
	}
}

// Run creates and starts a container for specified match
func (cs *containerServer) Run(match *matches.Match) (response RunContainerResponse, err error) {
	opts := prepareContainer(match)
	container, err := cs.docker.CreateContainer(opts)
	if err != nil {
		return
	}
	err = cs.docker.StartContainer(container.ID, opts.HostConfig)
	if err != nil {
		return
	}

	var portBindings []docker.PortBinding
	for _, v := range container.NetworkSettings.Ports {
		portBindings = v
		// lol yes
		// TODO: make this less fucking janky
		break
	}
	if len(portBindings) != 1 {
		err = fmt.Errorf("containers: expected one port binding, got %d", len(portBindings))
		return
	}
	port, err := strconv.ParseInt(portBindings[0].HostPort, 10, 64)
	if err != nil {
		return
	}

	response = RunContainerResponse{
		ContainerID: container.ID,
		Port:        port,
	}
	return
}

// Stop stops and removes specified container. If docker returns "no such
// container", eat the error and return.
func (cs *containerServer) Stop(containerID string) (err error) {
	err = cs.docker.StopContainer(containerID, StopServerTimeout)
	if err != nil {
		if _, ok := err.(*docker.NoSuchContainer); ok {
			err = nil
			return
		}
		return
	}
	return
}
