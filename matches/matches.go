package matches

import (
	"time"

	"bitbucket.org/nerdragegg/spawn/games"
)

// Match holds information about a match
type Match struct {
	ID             string    `json:"id"`
	Location       string    `json:"location,omitempty"`
	Image          string    `json:"image_id,omitempty"`
	JoinPassword   string    `json:"join_password,omitempty"`
	RCONPassword   string    `json:"rcon_password,omitempty"`
	Container      string    `json:"container_id,omitempty"`
	Agent          string    `json:"agent,omitempty"`
	HostPort       string    `json:"host_port,omitempty"`
	Game           string    `json:"game,omitempty"`
	StartTimestamp time.Time `json:"start_timestamp,omitempty"`
	EndTimestamp   time.Time `json:"end_timestamp,omitempty"`
}

// ServerOptions provides information for hosting a server for this game.
type ServerOptions struct {
	// Ports to be exposed from the container
	// map[port]<udp|tcp>
	Ports map[int64]string `json:"ports,omitempty"`

	// ID of docker image
	Image string `json:"image_id,omitempty"`

	// mount points inside container
	Volumes []string `json:"volumes,omitempty"`
}

// UrbanTerrorServerOptions are options for urban terror
var UrbanTerrorServerOptions = ServerOptions{
	Ports:   map[int64]string{27960: "udp"},
	Image:   "urbanterror/vanilla",
	Volumes: []string{"/UrbanTerror42/q3ut4/configs"},
}

// GetMatchServerOptions retrieves ServerOptions for a match
func GetMatchServerOptions(match *Match) ServerOptions {
	opts, _ := GetGameServerOptions(match.Game)
	return opts
}

// GetGameServerOptions is a lookup for ServerOptions by game id
func GetGameServerOptions(game string) (opts ServerOptions, ok bool) {
	opts, ok = gameToServerOptions[game]
	return
}

var gameToServerOptions = map[string]ServerOptions{
	games.UrbanTerror: UrbanTerrorServerOptions,
}
