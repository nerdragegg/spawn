# spawn

[ ![Codeship Status for
nerdragegg/spawn](https://codeship.com/projects/c1ea77c0-efee-0133-bcc2-721dba57ab13/status?branch=master)](https://codeship.com/projects/149125)

Spins up VMs to host urban terror servers.

## Spawn Agent vs Spawn API

### Spawn Agent

The spawn agent watches etcd for new match assignments in its location and
grabs lock if it has available resources. If successful, templates out server
configuration files using variables defined in the match vars key in etcd and
starts up a docker container running the game server. When the game server
is available, the agent registers game server's container ID, public IP address
and ephemeral host port assigned to the container. While the container is up,
the agent watches etcd for matches to be deleted. When the match directory is
deleted in etcd, the agent kills the container assigned to the match.

### Spawn API

The spawn api server provides an administrative interface for managing matches.
A JSON HTTP API is available. When a match is created, the spawn api server
writes to etcd to create the match directory and record the various
configuration variables needed by the agent to template out the game server
config.

## Match assignments

When a new match is requested, the spawn service will set
`/spawn/<location_id>/matches/<match_id>/match` in `etcd`. Agents in this
location will be watching for updates on `/spawn/<location_id>/matches/`
recursively. When an agent sees the `vars` key being created, the agent should
try to write its ID to `/spawn/<location_id>/matches/<match_id>/agent`, with
the assertion that the key must not exist and with a TTL of 30 seconds. This
serves as a lock representing the agent's reservation on providing a container
for this match.

When the container is started, the agent should refresh the TTL
on the `/agent` key. The agent should run a health check every 15 seconds to
ensure that the game server is up and refresh the TTL value on the `/agent` key
for the assigned match.

If `/spawn/<location_id>/matches/<match_id>/match` is deleted, then the agent
should kill the container serving the match and delete
`/spawn/<location_id>/matches/<match_id>/`.

Every agent should be watching every `<match_id>/` directory for changes.

### Match

| name | type | description |
| ---- | ---- | ----------- |
| `id` | `string` | unique match id |
| `location` | `string` | server location id |
| `image` | `string` | image id |
| `config_options` | `map[string]string` | variables to template out in config file (set in etcd, templated by agent before starting container) |
| `join_password` | `string` | generated join password |
| `rcon_password` | `string` | generated rcon password (not shown to user) |
| `container_id` | `string` | id of container running server for this match (set by agent in etcd) |
| `agent` | `string` | id of agent that picked up assignment (set by agent in etcd) |
| `host_port` | `string` | host:port of urban terror server (set by agent in etcd) |

### etcd:

#### `/spawn/<location_id>/agents/<agent_id>/`:

Agent registration with ttl of 1 hour.

#### `/spawn/<location_id>/agents/<agent_id>/network/ipv4`:

Public IPv4 address of agent's host.

#### `/spawn/<location_id>/agents/<agent_id>/containers/<container_id>/`:

Container registration with ttl of 10 minutes.

#### `/spawn/<location_id>/agents/<agent_id>/containers/<container_id>/port`:

Automatically assigned ephemeral port on the host proxying traffic to container.

#### `/spawn/<location_id>/matches/<match_id>/agent`:

Agent ID that picked up this match. If key is missing, then it's not assigned
yet.

#### `/spawn/<location_id>/matches/<match_id>/container`:

Container ID serving this match. If key is missing, then it's not assigned yet.

#### `/spawn/<location_id>/matches/<match_id>/match`:

Data about the match, including values to be passed into the config template.
