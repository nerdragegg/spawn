package spawn

import (
	"bitbucket.org/nerdragegg/spawn/events"
	"bitbucket.org/nerdragegg/spawn/kv"
)

// HandleLockableMatch tries to lock a match that's available
func (a *Agent) HandleLockableMatch(event *events.MatchEvent) (err error) {
	req := kv.LockMatchRequest{
		Match:    event.Match.ID,
		Location: a.config.Location,
		Agent:    a.id,
	}
	_, err = a.kv.LockMatch(a.ctx, req)
	if err != nil {
		return
	}
	return
}

// HandleCreatedMatch starts a watcher on a newly created match and tries to
// grab lock on the match.
func (a *Agent) HandleCreatedMatch(event *events.MatchEvent) (err error) {
	watchRequest := &kv.WatchMatchRequest{
		Events: a.events,
		Match:  event.Match,
	}
	err = a.kv.WatchMatch(a.ctx, watchRequest)
	if err != nil {
		return
	}

	err = a.HandleLockableMatch(event)
	if err != nil {
		return
	}
	return
}

// HandleAssignedMatch checks if the match has been assigned to
func (a *Agent) HandleAssignedMatch(event *events.MatchEvent) (err error) {
	if event.Match.Agent != a.id {
		Logger.Infof("spawn: match %s assigned to another agent: %s", event.Match.ID, event.Match.Agent)
		return
	}
	resp, err := a.cs.Run(event.Match)
	if err != nil {
		Logger.Errorf("spawn: error running container: %s", err.Error())
		return
	}

	request := kv.RegisterContainerRequest{
		Container: resp.ContainerID,
		Port:      resp.Port,
		TTL:       DefaultContainerTTL,
		Location:  a.config.Location,
		Agent:     a.id,
		Match:     event.Match.ID,
	}
	err = a.kv.RegisterContainer(a.ctx, request)
	if err != nil {
		Logger.Errorf("spawn: error registering container: %s", err.Error())
		return
	}
	return
}

// HandleDeletedMatch checks to see if the deleted match is owned by this agent
// and ensures the running container is stopped and deregistered in kv.
func (a *Agent) HandleDeletedMatch(event *events.MatchEvent) (err error) {
	if event.Match.Agent != a.id {
		Logger.Infof("spawn: match %s assigned to another agent: %s", event.Match.ID, event.Match.Agent)
		return
	}

	if event.Match.Container == "" {
		Logger.Infof("spawn: match object %s has no container associated with it, doing nothing", event.Match.ID)
		return
	}

	err = a.cs.Stop(event.Match.Container)
	if err != nil {
		return
	}

	request := kv.DeregisterContainerRequest{
		Container: event.Match.Container,
		Location:  a.config.Location,
		Agent:     a.id,
		Match:     event.Match.ID,
	}
	err = a.kv.DeregisterContainer(a.ctx, request)
	if err != nil {
		Logger.Errorf("spawn: error deregistering container: %s", err.Error())
		return
	}
	return
}
