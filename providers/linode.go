package providers

// TODO: implement capacity mappings

import (
	"net/http"
	"strconv"

	"github.com/taoh/linodego"
)

const (
	linodeInstanceSize   = 2
	linodeMonthlyBilling = 1
	linodeLatestKernel   = 138
	linodeUbuntuID       = 124
	linodeDiskSize       = 24448

	// LinodeProviderName is the name of the linode provider
	LinodeProviderName = "linode"
)

// Linode implements InstanceProvider
type Linode struct {
	client *linodego.Client
}

// NewLinodeProvider creates a new InstanceProvider for Linode
func NewLinodeProvider(accessKey string) *Linode {
	client := linodego.NewClient(accessKey, http.DefaultClient)
	return &Linode{client: client}
}

// regionID returns int representing datacenter id
func (l *Linode) regionID(region string) int {
	switch region {
	case NASC:
		return 2 // dallas
	case NACW:
		return 3 // fremont
	case NASE:
		return 4 // atlanta
	case NACE:
		return 6 // newark
	case EUNW:
		return 7 // london
	case APNE:
		return 8 // tokyo
	case APSE:
		return 9 // singapore
	case EUCC:
		return 10 // frankfurt
	default:
		return 0
	}
}

// given linode datacenter id, return spawn region uuid
func (l *Linode) region(id int) string {
	switch id {
	case 2:
		return NASC // dallas
	case 3:
		return NACW // fremont
	case 4:
		return NASE // atlanta
	case 6:
		return NACE // newark
	case 7:
		return EUNW // london
	case 8:
		return APNE // tokyo
	case 9:
		return APSE // singapore
	case 10:
		return EUCC // frankfurt
	default:
		return ""
	}
}

// CreateInstance creates a Linode instance
func (l *Linode) CreateInstance(instance *Instance) (err error) {
	if instance.Provider == "" {
		instance.Provider = LinodeProviderName
	}

	datacenterID := l.regionID(instance.Region)
	linode, err := l.client.Linode.Create(datacenterID, linodeInstanceSize, linodeMonthlyBilling)
	if err != nil {
		return
	}

	instance.ProviderID = strconv.Itoa(linode.LinodeId.LinodeId)

	ips, err := l.client.Ip.List(linode.LinodeId.LinodeId, -1)
	if err != nil {
		return
	}

	for _, v := range ips.FullIPAddresses {
		if v.IsPublic == 1 {
			instance.Address = v.IPAddress
			break
		}
	}

	diskID, password, err := l.createDisk(linode.LinodeId.LinodeId)
	if err != nil {
		return
	}

	instance.RootPassword = password

	configID, err := l.createConfig(linode.LinodeId.LinodeId, diskID)
	if err != nil {
		return
	}

	_, err = l.client.Linode.Boot(linode.LinodeId.LinodeId, configID)
	if err != nil {
		return
	}
	return
}

func (l *Linode) createDisk(linodeID int) (diskID, password string, err error) {
	password = GeneratePassword()

	args := make(map[string]string)
	args["rootPass"] = password
	resp, err := l.client.Disk.CreateFromDistribution(linodeUbuntuID, linodeID, "spawn", linodeDiskSize, args)
	if err != nil {
		return
	}
	diskID = strconv.Itoa(resp.DiskJob.DiskId)
	return
}

func (l *Linode) createConfig(linodeID int, diskID string) (id int, err error) {
	args := make(map[string]string)
	args["DiskList"] = diskID
	args["RootDeviceNum"] = "1"
	args["helper_distro"] = "true"
	args["helper_network"] = "true"
	resp, err := l.client.Config.Create(linodeID, linodeLatestKernel, "spawn", args)
	if err != nil {
		return
	}
	id = resp.LinodeConfigId.LinodeConfigId
	return
}

// DeleteInstance removes a Linode instance
func (l *Linode) DeleteInstance(instance *Instance) (err error) {
	linodeID, err := strconv.Atoi(instance.ProviderID)
	if err != nil {
		return
	}

	_, err = l.client.Linode.Delete(linodeID, true)
	if err != nil {
		return
	}
	return
}
