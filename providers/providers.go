package providers

import "github.com/cmiceli/password-generator-go"

// region UUIDs
// C: central
// N: north
// S: south
// W: west
// E: east
//
// NA: North America
// SA: South America
// EU: Europe
// AP: Asia Pacific
const (
	NACW = "4b58721e-3e7a-4a32-bc70-aa4212a55e49"
	NANW = "0c83583a-d620-4dc4-9423-ca023ed024ce"
	NASW = "66ac18e6-f2c1-4765-8e10-28e3e524a20e"
	NASC = "c4f8a7a0-4255-4eb2-a49d-ef4c275ec61d"
	NANC = "b5f79bfb-3fc8-425f-9ba8-456ec8f40240"
	NANE = "614fc7df-737b-410a-a5c4-983ac88a86a7"
	NACE = "51deb154-c9be-4dc9-8cd4-0d58c4714e2a"
	NASE = "fe097244-dcde-483f-9550-b7253385072a"
	EUCC = "a0cd38fb-03a0-4ece-b5e1-98dbb48b2566"
	EUNW = "6a4f4d33-7396-430d-a75e-a6ce6c7df6c4"
	SACE = "6b1792b5-473b-4626-957f-ec4d356b23ae"
	APNE = "10a9cb2d-be34-44f1-96f0-719db7d85d3d"
	APSE = "90e216e1-89f9-4f0c-9ac0-5b4baee0f9aa"
)

// password length
const (
	DefaultPasswordLength = 30
)

// InstanceProvider describes the interface for managing instances for spawn.
type InstanceProvider interface {
	CreateInstance(instance *Instance) error
	DeleteInstance(instance *Instance) error
}

// Instance is a single VM object.
type Instance struct {
	ID           string `json:"id"`
	ProviderID   string `json:"provider_id,omitempty"`
	Provider     string `json:"provider,omitempty"`
	Region       string `json:"region,omitempty"`
	Address      string `json:"host,omitempty"`
	Capacity     int64  `json:"capacity,omitempty"`
	RootPassword string `json:"root_password,omitempty"`
}

// GeneratePassword returns a random string to be used as a password
func GeneratePassword() string { return pwordgen.NewPassword(DefaultPasswordLength) }
