package events

import "bitbucket.org/nerdragegg/spawn/matches"

// MatchEvent is returned from watching matches
type MatchEvent struct {
	Match    *matches.Match     `json:"match,omitempty"`
	Scenario MatchEventScenario `json:"scenario,omitempty"`
}

// MatchEventHandler processes a match event, is registered with
// MatchEventRouter
type MatchEventHandler func(event *MatchEvent) (err error)

// MatchEventScenario describes the event
type MatchEventScenario string

// Event types
const (
	// match created, ready for an agent to pick up. agents should start
	// watching this match and try to grab lock.
	MatchCreated MatchEventScenario = "MatchCreated"

	// match deleted, agent assigned to this match should handle match deletion
	// and agents watching this match should stop watching it.
	MatchDeleted = "MatchDeleted"

	// an agent has grabbed lock on this match.
	MatchAssigned = "MatchAssigned"

	// the agent that previously had lock on this match has dropped lock.
	// agents should try to grab lock on this match.
	MatchUnassigned = "MatchUnassigned"

	// an event handled that is so far unhandled
	MatchStateUnknown = "MatchStateUnknown"
)

// MatchEventRouter inspects the MatchEvent and calls the registered event
// handler with the MatchEvent.
type MatchEventRouter interface {
	Register(scenario MatchEventScenario, handler MatchEventHandler)
	HandleEvent(event *MatchEvent) (err error)
}

// DefaultMatchEventRouter is the package-level MatchEventRouter
var DefaultMatchEventRouter = &matchEventRouter{
	handlers: make(map[MatchEventScenario]MatchEventHandler),
}

// implements MatchEventRouter
type matchEventRouter struct {
	handlers map[MatchEventScenario]MatchEventHandler
}

// Register registers the MatchEventHandler. MatchEventRouter will call handler
// when an event happens that matches the handler's MatchEventScenario.
func (mer *matchEventRouter) Register(scenario MatchEventScenario, handler MatchEventHandler) {
	mer.handlers[scenario] = handler
}

// HandleEvent calls registered handler based on the Scenario.
func (mer *matchEventRouter) HandleEvent(event *MatchEvent) (err error) {
	if event == nil {
		// noop
		return
	}

	var handler MatchEventHandler
	var ok bool
	if handler, ok = mer.handlers[event.Scenario]; !ok {
		return
	}

	err = handler(event)
	if err != nil {
		return
	}
	return
}
