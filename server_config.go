package spawn

import (
	"bytes"
	"errors"
	"io"
	"net/http"
	"text/template"

	"github.com/cmiceli/password-generator-go"
)

// DefaultURTPasswordLength is the default rcon/join password lengths for urban
// terror servers
const DefaultURTPasswordLength = 10

// DefaultConfigTemplateURL is the URL for the ftwgl server config template
const DefaultConfigTemplateURL = "https://storage.googleapis.com/urbanterror/ftwgl/server.cfg.tmpl"

var configTemplate string

// DefaultServerConfig returns a server config with default values
func DefaultServerConfig() ServerConfig {
	return ServerConfig{
		MatchID:      "unassigned",
		BlueTeam:     "Blue Team",
		RedTeam:      "Red Team",
		Location:     "Internet",
		RCONPassword: pwordgen.NewPassword(DefaultURTPasswordLength),
		JoinPassword: pwordgen.NewPassword(DefaultURTPasswordLength),
	}
}

// ServerConfig holds template data for urban terror server config
type ServerConfig struct {
	MatchID      string `json:"match_id,omitempty"`
	BlueTeam     string `json:"blue_team,omitempty"`
	RedTeam      string `json:"red_team,omitempty"`
	Location     string `json:"location,omitempty"`
	RCONPassword string `json:"rcon_password,omitempty"`
	JoinPassword string `json:"join_password,omitempty"`
	Map          string `json:"map,omitempty"`
}

// Render templates out a server config based on template values
func (sc *ServerConfig) Render() (config string, err error) {
	if configTemplate == "" {
		configTemplate, err = fetchTemplate(DefaultConfigTemplateURL)
		if err != nil {
			return
		}
	}

	t, err := template.New("server_config").Parse(configTemplate)
	if err != nil {
		return
	}

	var w *bytes.Buffer
	err = t.Execute(w, sc)
	if err != nil {
		return
	}

	config = w.String()
	return
}

func fetchTemplate(templateURL string) (template string, err error) {
	response, err := http.Get(templateURL)
	if err != nil {
		return
	}
	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		err = errors.New("unable to fetch server config template")
		return
	}

	b := make([]byte, response.ContentLength)
	_, err = io.ReadFull(response.Body, b)
	if err != nil {
		return
	}

	template = string(b)
	return
}
